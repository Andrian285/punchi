import React from 'react'
import Config from './config'
import { createStore, applyMiddleware } from 'redux'
import reducer from './reducers'
import Navigator from './navigator'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
const store = createStore(reducer, applyMiddleware(thunk))

// Scheduled battles | Daily challenges

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Navigator />
            </Provider>
        )
    }
}
