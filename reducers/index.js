import { combineReducers } from 'redux'
import auth from './auth'
import tabs from './tabs'

const reducer = combineReducers({
    auth,
    tabs
})

export default reducer