import { SET_PAGE_LOADED } from '../actions/tabs'

export default function tabsReducer(state = [], action) {
    switch (action.type) {
        case SET_PAGE_LOADED:
            return { ...state, loaded: action.loaded }
        default:
            return state
    }
}