import {
    SET_REFRESH,
    REQUEST_AUTH,
    REQUEST_AUTH_ERROR,
    REQUEST_AUTH_SUCCESS,
    REQUEST_SIGN_IN_SUCCESS,
    REQUEST_SIGN_UP_SUCCESS,
    RESET_REDIRECTION_TO_SIGN_IN,
    RESET_ERROR
} from '../actions/auth'

export default function authReducer(state = [], action) {
    switch (action.type) {
        case SET_REFRESH:
            return { ...state, refresh: action.refresh }
        case RESET_REDIRECTION_TO_SIGN_IN:
            return { ...state, redirectToSignIn: action.redirectToSignIn }
        case RESET_ERROR:
            return { ...state, error: action.error }
        case REQUEST_AUTH:
            return { ...state, loading: action.loading }
        case REQUEST_AUTH_ERROR:
            return { ...state, loading: action.loading, error: action.error }
        case REQUEST_AUTH_SUCCESS:
            return { ...state, loading: action.loading, error: action.error }
        case REQUEST_SIGN_IN_SUCCESS:
            return { ...state, loading: action.loading, error: action.error, refresh: action.refresh }
        case REQUEST_SIGN_UP_SUCCESS:
            return { ...state, loading: action.loading, error: action.error, redirectToSignIn: action.redirectToSignIn }
        default:
            return state
    }
}