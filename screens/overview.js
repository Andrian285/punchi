import React from 'react'
import { StyleSheet, Text, View, Dimensions } from 'react-native'
import { Button } from 'react-native-elements'
import { createStackNavigator } from 'react-navigation'
// import Parallax from '../components/parallax'
import Config from '../config'
import SignInScreen from './stack/signin'
import SignUpScreen from './stack/signup'
import SliderItem from '../components/overview/slider-item'
import Carousel from 'react-native-snap-carousel'
import NavBar from '../components/navbar'

const entries = [
    {
        title: 'Become a famous rapper',
        subtitle: 'Sign up for free and become a real rapper. Organise battles with other rappers, pass daily challenges and do any fucking thing you want to.',
        source: require('../assets/images/rapper2.jpg')
    },
    {
        title: 'Become a judge',
        subtitle: 'Fill in the application and become a real judge. Now you may judge any battle you want to and blah blah blah.',
        source: require('../assets/images/judge.jpg')
    },
]

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window')


class OverviewScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View>
                <NavBar />
                <Content {...this.props} />
            </View>
        )
    }
}

export default createStackNavigator(
    {
        Overview: OverviewScreen,
        SignIn: SignInScreen,
        SignUp: SignUpScreen
    },
    {
        initialRouteName: 'Overview',
    }
)

class Content extends React.Component {
    renderItem({ item, index }) {
        return (
            <SliderItem data={item} />
        )
    }

    render() {
        return (
            <View>
                <Carousel
                    data={entries}
                    renderItem={this.renderItem}
                    sliderWidth={viewportWidth}
                    itemWidth={viewportWidth}
                />
                <Button
                    title='Sign up'
                    buttonStyle={styles.button}
                    onPress={() => this.props.navigation.navigate('SignUp')}
                />
                <Button
                    title='Sign in'
                    buttonStyle={styles.button}
                    onPress={() => this.props.navigation.navigate('SignIn')}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: Config.themeColor,
        marginTop: 10
    },
    titleParallax: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18,
    },
    tileDescription: {
        fontSize: 15,
        margin: 5,
    }
})