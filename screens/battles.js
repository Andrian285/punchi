import React from 'react'
import { StyleSheet, View } from 'react-native'
import { Tile, Text } from 'react-native-elements'
import Config from '../config'
import NavBar from '../components/navbar'

const BattleTile = () => {
    return (
        <View>
            <Tile
                imageSrc={require('../assets/images/rapper.jpg')}
                // title="Lorem ipsum dolor sit amet, consectetur"
                icon={{ name: 'play-circle', type: 'font-awesome', color: Config.themeColor + '80', size: 80 }} // optional
                contentContainerStyle={{ height: 70 }}
                featured
            />
            <View style={styles.info}>
                <Text style={styles.infoTitle}>Super mega battle</Text>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <Text style={{ fontStyle: 'italic' }}>Eminem</Text>
                    <Text style={{ fontWeight: 'bold' }}>{'  '}VS{'  '}</Text>
                    <Text style={{ fontStyle: 'italic' }}>Lil Pump</Text>
                </View>
            </View>
        </View>
    )
}

export default class BattlesScreen extends React.Component {
    render() {
        return (
            <View>
                <NavBar />
                <BattleTile />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    info: {
        borderWidth: 2,
        borderColor: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        height: 60
    },
    infoTitle: {
        fontSize: 25,
        fontWeight: 'bold'
    }
})