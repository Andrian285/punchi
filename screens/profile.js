import React from 'react'
import { StyleSheet, Text, View, AsyncStorage, Alert, ActivityIndicator } from 'react-native'
import { Button } from 'react-native-elements'
import Config from '../config'
import NavBar from '../components/navbar'
import { setRefresh } from '../actions/auth'
import { connect } from 'react-redux'

class Profile extends React.Component {
    state = {
        user: null
    }

    async componentDidMount() {
        try {
            const token = await AsyncStorage.getItem('@app:token');
            if (token !== null) {
                let response = await fetch(Config.fetchURI + Config.fetchPath.profile, {
                    method: 'GET',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        token
                    }
                })
                let responseJson = await response.json()
                if (response.ok) this.setState({ user: responseJson.user })
                else {
                    if (response.status === 401) await this.logout()
                    else throw new Error(responseJson)
                }
            }
        } catch (err) {
            Alert.alert(
                'ERROR',
                err.message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false }
            )
        }
    }

    async logout() {
        await AsyncStorage.removeItem('@app:token')
        this.props.setRefresh(true)
    }

    render() {
        return (
            <View>
                <NavBar />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <View style={styles.card}>
                        {this.state.user ?
                            <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', marginLeft: 10 }}>
                                <Text>Username: {this.state.user.username}</Text>
                                <Text>Email: {this.state.user.email}</Text>
                            </View>
                            :
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <ActivityIndicator size={20} color={Config.themeBarColor} />
                            </View>
                        }
                    </View>
                </View>

                <Button title='Logout' onPress={() => this.logout()} buttonStyle={styles.logout} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        width: '90%',
        marginBottom: 10,
        marginTop: 10,
        borderWidth: 1,
        borderRadius: 3,
        borderColor: '#D6D4D4',
        paddingTop: 5,
        paddingBottom: 5
    }
})

const mapDispatchToProps = {
    setRefresh
}
const ProfileScreen = connect(null, mapDispatchToProps)(Profile)

export default ProfileScreen