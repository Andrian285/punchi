import React from 'react'
import { StyleSheet, Text, View, Platform } from 'react-native'
import Config from '../config'
import NavBar from '../components/navbar'

export default class HomeScreen extends React.Component {
    render() {
        return (
            <View>
                <NavBar />
                <View>
                    <Text>Kek</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: Config.themeColor,
        borderRadius: 5
    }
})