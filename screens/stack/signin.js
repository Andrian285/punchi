import React from 'react'
import validator from 'email-validator'
import Config from '../../config'
import Form from '../../components/form'
import { fetchSignIn } from '../../actions/auth'
import { connect } from 'react-redux'

const fields = [
    {
        label: 'Email',
        err: 'The email address you supplied is invalid.',
        requirement: email => validator.validate(email)
    },
    {
        label: 'Password',
        err: 'Invalid password: must be at least 8 characters',
        requirement: pass => pass.length >= 8,
        isPassword: true
    },
]

class SignIn extends React.Component {
    static navigationOptions = {
        title: 'SIGN IN',
        headerStyle: {
            backgroundColor: Config.themeBarColor,
            height: Config.navbarHeight,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: Config.navbarFontSize,
            alignItems: 'center'
        },
    }

    render() {
        return (
            <Form fields={fields} fetchFunc={this.props.fetchSignIn} />
        )
    }
}

const mapDispatchToProps = {
    fetchSignIn
}
const SignInScreen = connect(null, mapDispatchToProps)(SignIn)

export default SignInScreen
