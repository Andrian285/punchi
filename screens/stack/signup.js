import React from 'react'
import validator from 'email-validator'
import Config from '../../config'
import Form from '../../components/form'
import { fetchSignUp, resetRedirectionToSignIn } from '../../actions/auth'
import { connect } from 'react-redux'

const fields = [
    {
        label: 'Username',
        err: 'Invalid Username: must be at least 3 characters',
        requirement: username => username.length >= 3
    },
    {
        label: 'Email',
        err: 'The email address you supplied is invalid.',
        requirement: email => validator.validate(email)
    },
    {
        label: 'Password',
        err: 'Invalid password: must be at least 8 characters',
        requirement: pass => pass.length >= 8,
        isPassword: true
    },
    {
        label: 'Confirm password',
        err: 'Passwords don\'t match.',
        requirement: (confirm, pass) => confirm === pass,
        isPassword: true
    },
]

class SignUp extends React.Component {
    static navigationOptions = {
        title: 'SIGN UP',
        headerStyle: {
            backgroundColor: Config.themeBarColor,
            height: Config.navbarHeight,
            shadowOpacity: 0,
            shadowOffset: {
                height: 0,
            },
            shadowRadius: 0,
            elevation: 0
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            fontWeight: 'bold',
            fontSize: Config.navbarFontSize,
            alignItems: 'center'
        },
    }

    componentDidUpdate() {
        if (this.props.redirectToSignIn) {
            this.props.resetRedirectionToSignIn()
            this.props.navigation.navigate('SignIn')
        }
    }

    render() {
        return (
            <Form fields={fields} fetchFunc={this.props.fetchSignUp} />
        )
    }
}

const mapStateToProps = (state) => ({
    redirectToSignIn: state.auth.redirectToSignIn
})

const mapDispatchToProps = {
    fetchSignUp,
    resetRedirectionToSignIn
}
const SignUpScreen = connect(mapStateToProps, mapDispatchToProps)(SignUp)

export default SignUpScreen
