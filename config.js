import { Platform } from 'react-native'

const themeBarColor = "#3A0934"
const themeColor = "#6A0B5E"
const navbarHeight = Platform.OS === 'ios' ? 70 : 70 - 24
const navbarFontSize = 14.5

let errStatus = [500, 400, 401, 403]
const fetchURI = 'https://punchiapp.herokuapp.com/'
// 'https://punchiapp.herokuapp.com/'
// 'http://192.168.1.6:8080/'
const fetchPath = {
    signin: 'api/auth/signin',
    signup: 'api/auth/signup',
    authentication: 'api/auth/authenticate',
    profile: 'api/profile/'
}

export default {
    themeColor,
    themeBarColor,
    navbarHeight,
    navbarFontSize,
    fetchURI,
    fetchPath,
    errStatus
}