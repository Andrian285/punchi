export const SET_PAGE_LOADED = 'SET_PAGE_LOADED'

export function setPageLoaded(loaded) {
    return {
        type: SET_PAGE_LOADED,
        loaded
    }
}