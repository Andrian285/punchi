export function defaultActionCreator(type, ...argNames) {
    return function (...args) {
        const action = { type }
        argNames.forEach((arg, index) => {
            action[argNames[index]] = args[index]
        })
        return action
    }
}

export function advancedActionCreator(type, definedValuesObj, ...argNames) {
    return function (...args) {
        const action = { type, ...definedValuesObj }
        argNames.forEach((arg, index) => {
            action[argNames[index]] = args[index]
        })
        return action
    }
}

export function asyncActionCreator(url, cbProcessing, cbSuccess, cbErr, additionalSuccessActions) {
    return function (body) {
        return async function (dispatch) {
            dispatch(cbProcessing())
            try {
                let response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(body),
                })
                let responseJson = await response.json()
                if (response.ok) {
                    if (additionalSuccessActions) additionalSuccessActions(responseJson)
                    dispatch(cbSuccess(...Object.values(responseJson)))
                }
                else dispatch(cbErr(responseJson.message))
            } catch (error) {
                dispatch(cbErr(error.message))
            }
        }
    }
}