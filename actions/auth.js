import Config from '../config'
import { defaultActionCreator, advancedActionCreator, asyncActionCreator } from './actionCreator'
import { AsyncStorage } from 'react-native'

export const SET_REFRESH = 'SET_REFRESH'
export const setRefresh = defaultActionCreator(SET_REFRESH, 'refresh')

export const RESET_REDIRECTION_TO_SIGN_IN = 'RESET_REDIRECTION_TO_SIGN_IN'
export const resetRedirectionToSignIn = advancedActionCreator(RESET_REDIRECTION_TO_SIGN_IN, { redirectToSignIn: false })

export const RESET_ERROR = 'RESET_ERROR'
export const resetError = advancedActionCreator(RESET_ERROR, { error: null })


// ASYNC API REQUESTS
export const REQUEST_AUTH = 'REQUEST_AUTH'
export const requestAuth = advancedActionCreator(REQUEST_AUTH, { loading: true })

export const REQUEST_AUTH_ERROR = 'REQUEST_AUTH_ERROR'
export const requestAuthError = advancedActionCreator(REQUEST_AUTH_ERROR, { loading: false }, 'error')

export const REQUEST_AUTH_SUCCESS = 'REQUEST_AUTH_SUCCESS'
export const requestAuthSuccess = advancedActionCreator(REQUEST_AUTH_SUCCESS, { loading: false, error: null })

export const REQUEST_SIGN_IN_SUCCESS = 'REQUEST_SIGN_IN_SUCCESS'
export const requestSignInSuccess = advancedActionCreator(REQUEST_SIGN_IN_SUCCESS, { loading: false, error: null, refresh: true })

export const REQUEST_SIGN_UP_SUCCESS = 'REQUEST_SIGN_UP_SUCCESS'
export const requestSignUpSuccess = advancedActionCreator(REQUEST_SIGN_UP_SUCCESS, { loading: false, error: null, redirectToSignIn: true })


export const fetchSignIn = asyncActionCreator(
    Config.fetchURI + Config.fetchPath.signin,
    requestAuth,
    requestSignInSuccess,
    requestAuthError,
    async (responseJson) => await AsyncStorage.setItem('@app:token', responseJson.token) // insert in the future catching error for application security
)

export const fetchSignUp = asyncActionCreator(
    Config.fetchURI + Config.fetchPath.signup,
    requestAuth,
    requestSignUpSuccess,
    requestAuthError
)