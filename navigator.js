import React from 'react'
import { Ionicons } from '@expo/vector-icons'
import { createBottomTabNavigator } from 'react-navigation'
import Config from './config'
import OverviewScreen from './screens/overview'
import BattlesScreen from './screens/battles'
import DailyChallengesScreen from './screens/challenges'
import ProfileScreen from './screens/profile'
import HomeScreen from './screens/home'
import { Alert, AsyncStorage, View, Text, ActivityIndicator, ImageBackground, StyleSheet } from 'react-native'
import { setRefresh } from './actions/auth'
import { setPageLoaded } from './actions/tabs'
import { connect } from 'react-redux'

let screens = null

const unregisteredScreens = {
    Overview: OverviewScreen,
    Battles: BattlesScreen,
    Challenges: DailyChallengesScreen,
}

const registeredScreens = {
    Home: HomeScreen,
    Battles: BattlesScreen,
    Challenges: DailyChallengesScreen,
    Profile: ProfileScreen,
}

const navigatorOpts = {
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state
            let iconName
            switch (routeName) {
                case 'Overview':
                    iconName = `ios-information-circle${focused ? '' : '-outline'}`
                    break
                case 'Battles':
                    iconName = `ios-flash${focused ? '' : '-outline'}`
                    break
                case 'Challenges':
                    iconName = `ios-pulse${focused ? '' : '-outline'}`
                    break
                case 'Profile':
                    iconName = `ios-contact${focused ? '' : '-outline'}`
                    break
                case 'Home':
                    iconName = `ios-home${focused ? '' : '-outline'}`
                    break
                default:
                    iconName = `ios-alert${focused ? '' : '-outline'}`
                    break
            }

            return <Ionicons name={iconName} size={25} color={tintColor} />
        },
    }),
    tabBarOptions: {
        activeTintColor: Config.themeColor,
        inactiveTintColor: 'gray',
    },
}

class App extends React.Component {
    state = {
        navigator: null,
        loaded: false,
        splashScreenWasShown: false,
        splashScreenLoaded: false
    }

    splashScreenImageLoaded() {
        this.setState({ splashScreenLoaded: true })
        setTimeout(() => this.setTabBarScreens(), 3000)
    }

    async setTabBarScreens() {
        this.props.setRefresh(false)
        if (this.state.loaded) this.setState({ loaded: false })
        screens = unregisteredScreens
        try {
            const token = await AsyncStorage.getItem('@app:token')
            if (token !== null) screens = registeredScreens
            this.setState({ navigator: createBottomTabNavigator(screens, navigatorOpts), loaded: true, splashScreenWasShown: true })
        } catch (err) {
            console.log(err)
            this.setState({ navigator: createBottomTabNavigator(unregisteredScreens, navigatorOpts), loaded: true, splashScreenWasShown: true })
            Alert.alert(
                'ERROR',
                err.message,
                [
                    { text: 'OK' },
                ],
                { cancelable: false }
            )
        }
    }

    componentDidUpdate() {
        if (this.props.refresh) this.setTabBarScreens()
    }

    render() {
        if (this.state.loaded) return <this.state.navigator />
        return (
            this.state.splashScreenWasShown ?
                <View style={styles.loadingScreenContainer}>
                    <ActivityIndicator size={80} color={Config.themeBarColor} />
                </View>
                :
                <ImageBackground
                    source={require('./assets/images/splash_screen.png')}
                    style={styles.loadingScreenContainer}
                    onLoad={() => this.splashScreenImageLoaded()}
                >
                    {this.state.splashScreenLoaded && <ActivityIndicator size={80} color='black' style={{ position: 'absolute', bottom: 20 }} />}
                </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    loadingScreenContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

const mapStateToProps = (state) => ({
    refresh: state.auth.refresh,
    loaded: state.tabs.loaded
})

const mapDispatchToProps = {
    setRefresh,
    setPageLoaded
}
const Main = connect(mapStateToProps, mapDispatchToProps)(App)

export default Main
