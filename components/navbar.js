import React from 'react'
import { StyleSheet, StatusBar, View, Platform, TextInput } from 'react-native'
import Config from '../config'
import { SearchBar, Button, Text } from 'react-native-elements'
import { Ionicons } from '@expo/vector-icons'

export default class NavBar extends React.Component {
    state = {
        displaySearch: false
    }

    render() {
        return (
            <View style={{ backgroundColor: Config.themeBarColor, height: Config.navbarHeight + StatusBar.currentHeight }}>
                {this.state.displaySearch ?
                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Button
                            icon={{ name: 'arrow-back', color: '#fff', size: 30 }}
                            buttonStyle={styles.backButton}
                            onPress={() => this.setState({ displaySearch: false })}
                            containerViewStyle={{ flex: 1, marginLeft: 0 }}
                        />
                        <View style={{ flex: 5, flexDirection: 'row', backgroundColor: '#32072C', marginTop: StatusBar.currentHeight }}>
                            <TextInput
                                style={styles.searchBar}
                                placeholder='Search smth...'
                                underlineColorAndroid='transparent'
                                autoFocus
                                clearButtonMode='always'
                                ref='searchField'
                            // onChangeText={(text) => this.setState({ text })}
                            // value={this.state.text}
                            />
                            <Ionicons name='ios-close' size={40} color='#fff' onPress={() => this.refs.searchField.clear()} />
                        </View>
                    </View>
                    :
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.title}>PUNCHI</Text>
                        <Button
                            icon={{ name: 'search', color: '#fff', size: 30 }}
                            buttonStyle={styles.searchButton}
                            onPress={() => this.setState({ displaySearch: true })}
                        />
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    searchButton: {
        marginTop: 30,
        width: 70,
        height: 40,
        justifyContent: 'flex-end',
        alignItems: 'center',
        right: 0,
        backgroundColor: 'transparent'
    },
    title: {
        fontSize: Config.navbarFontSize,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: Platform.OS === 'ios' ? 28 : 38,
        marginLeft: 35,
        color: '#fff'
    },
    backButton: {
        marginTop: StatusBar.currentHeight,
        height: Config.navbarHeight,
        backgroundColor: 'transparent'
    },
    searchBar: {
        height: Config.navbarHeight,
        width: '90%',
        backgroundColor: '#32072C',
        paddingLeft: 20,
        paddingRight: 10,
        color: '#fff'
    }
})