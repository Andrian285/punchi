import React, { Component } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Tile } from 'react-native-elements'

export default class SliderItem extends Component {
    render() {
        return (
            <Tile
                imageSrc={this.props.data.source}
                title={this.props.data.title}
                caption={this.props.data.subtitle}
                activeOpacity={1}
                featured
            />
        )
    }
}