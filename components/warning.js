import React from 'react'
import Config from '../config'
import { StyleSheet, View, Text } from 'react-native'

const Warning = (props) => {
    return (
        props.message &&
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <View style={styles.container}>
                <Text style={styles.text}>
                    {props.message}
                </Text>
            </View>
        </View>
    )
}

export default Warning

const styles = StyleSheet.create({
    container: {
        borderWidth: 1,
        borderColor: '#6F3E3E',
        backgroundColor: '#F5A8A8',
        width: '90%',
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
        minHeight: 80,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10
    },
    text: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#6F3E3E'
    }
})
