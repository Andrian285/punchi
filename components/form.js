import React from 'react'
import { View, ScrollView } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
import Config from '../config'
import Warning from './warning'
import { connect } from 'react-redux'
import { fetchSignIn, resetError } from '../actions/auth'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view' // ne rabotae bleat

const Field = (props) => {
    return (
        <View>
            <FormLabel>{props.label}</FormLabel>
            <FormInput onChangeText={props.onChange} onFocus={props.onFocus} secureTextEntry={props.isPassword} />
            <FormValidationMessage>{props.displayErr && props.err}</FormValidationMessage>
        </View>
    )
}

class Form extends React.Component {
    state = {
        visibleErrors: [],
        data: [],
        loading: false
    }

    componentDidMount() {
        let data = this.state.data.slice()
        for (let field of this.props.fields)
            data.push({
                field,
                text: ''
            })
        this.setState({ data })
    }

    async fetchData() {
        let body = {}
        this.state.data.map(elm => body[elm.field.label.toLowerCase()] = elm.text)
        this.props.fetchFunc(body)
    }

    toggleErrs(errs, field, ...args) {
        if (!errs.includes(field.label) && !field.requirement(...args)) errs.push(field.label)
        else if (errs.includes(field.label) && field.requirement(...args)) errs.splice(errs.indexOf(field.label), 1)
    }

    finalValidation() {
        let errs = this.state.visibleErrors.slice()
        let data = this.state.data.slice()
        for (let elm of data) {
            if (elm.field.label.toLowerCase().includes('confirm'))
                this.toggleErrs(errs, elm.field, elm.text, data[data.findIndex(x => x.field.label.toLowerCase() === 'password')].text)
            else this.toggleErrs(errs, elm.field, elm.text)
        }
        this.setState({ visibleErrors: errs })

        if (errs.length === 0) this.fetchData()
    }

    addData(text, field) {
        let data = this.state.data.slice()
        let ind = data.findIndex(elm => elm.field.label === field.label)
        data[ind].text = text
        this.setState({ data })
    }

    componentWillUnmount() {
        if (this.props.error) this.props.resetError()
    }

    render() {
        return (
            <ScrollView>
                <Warning message={this.props.error ? this.props.error : null} />
                <View>
                    {this.props.fields.map((field, index) =>
                        <Field
                            {...field}
                            key={index}
                            onChange={(text) => this.addData(text, field)}
                            displayErr={this.state.visibleErrors.includes(field.label)}
                        />
                    )}
                </View>
                <View style={{ height: 50, marginTop: 20, marginBottom: 10 }}>
                    <Button
                        buttonStyle={{ height: 50, backgroundColor: 'green' }}
                        title='Submit'
                        onPress={() => this.finalValidation()}
                        loading={this.props.loading}
                        disabled={this.props.loading}
                    />
                </View>
            </ScrollView>
        )
    }
}

const mapStateToProps = (state) => ({
    loading: state.auth.loading,
    error: state.auth.error
})

const mapDispatchToProps = {
    fetchSignIn,
    resetError
}
const App = connect(mapStateToProps, mapDispatchToProps)(Form)

export default App
